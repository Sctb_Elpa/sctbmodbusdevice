cmake_minimum_required(VERSION 2.8)

project(Probe)

# указать путь до модулей
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
	"${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake_modules/")

INCLUDE(check_c11support)
CHECK_C11_SUPPORT()

find_package(SCTBModbusDevice REQUIRED)

include_directories(${SCTBModbusDevice_INCLUDE_DIRS})

#список сырцов
set(${PROJECT_NAME}_SRCS
    ../main.cpp
    )

add_executable(${PROJECT_NAME}
    ${${PROJECT_NAME}_SRCS}
    )

# импортируется как знание где взять SCTBModbusDevice_static так и
# его инклюды
target_link_libraries(${PROJECT_NAME} SCTBModbusDevice)
