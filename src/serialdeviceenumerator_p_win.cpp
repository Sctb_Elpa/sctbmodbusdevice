/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/

#include <vector>
#include <cassert>

#include <objbase.h>
#include <initguid.h>
#include <setupapi.h>

#include "serialdeviceenumerator.h"
#include "serialdeviceenumerator_p.h"

#include "common.h"
#include "Variant.h"

#ifdef USE_C11
#include <regex>
#endif

#ifndef NDEBUG
#include <iostream>
#define qDebug() std::cout
#endif

using namespace SCTBModbusDevice;

SerialDeviceEnumeratorPrivate::SerialDeviceEnumeratorPrivate()
{
    this->eHandle = ::CreateEvent(0, false, false, 0);

    if (!this->eHandle) {
#ifdef qDebug
	qDebug() << "Windows: SerialDeviceEnumeratorPrivate::setEnabled(..) \n"
		    " -> function: ::CreateEvent(..) returned: 0 \n"
		    " -> last error: " << ::GetLastError() << ". Error! \n";
#endif
	return;
    }

    String subKey = "SYSTEM\\CurrentControlSet\\services";
    ::LONG ret = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
				subKey.c_str(),
				0,
				KEY_NOTIFY,
				&this->keyHandle);

    if (ERROR_SUCCESS != ret) {
	this->keyHandle = 0;
#ifdef qDebug
	qDebug() << "Windows: SerialDeviceEnumeratorPrivate::setEnabled(..) \n"
		    " -> function: ::RegOpenKeyEx(..) returned: "
		 << ret << ". Error! \n";
#endif
	return;
    }
}

SerialDeviceEnumeratorPrivate::~SerialDeviceEnumeratorPrivate()
{
    if (this->eHandle)
	::CloseHandle(this->eHandle);

    if (this->keyHandle)
	::RegCloseKey(this->keyHandle);
}

bool SerialDeviceEnumeratorPrivate::nativeIsBusy() const
{
    bool ret = false;
    String path = this->nativeName();
    if (path.empty())
	return ret;

    path.insert(0, "\\\\.\\");

    ::HANDLE hd = ::CreateFile(path.c_str(),
			       GENERIC_READ | GENERIC_WRITE,
			       0,
			       0,
			       OPEN_EXISTING,
			       0,
			       0);

    if (INVALID_HANDLE_VALUE == hd) {
	ret = true;

	::LONG err = ::GetLastError();
	switch (err) {
	case ERROR_ACCESS_DENIED:
#ifdef qDebug
	    qDebug() << "Windows: SerialDeviceEnumeratorPrivate::isBusy() \n"
			" -> function: ::GetLastError() returned "
		     << err << " \n ie Access is denied. \n";
#endif
	    break;
	default:
#ifdef qDebug
	    qDebug() <<"Windows: SerialDeviceEnumeratorPrivate::isBusy() \n"
		       " -> function: ::GetLastError() returned "
		    << err << " \n ie unknown error. \n";
#endif
	    ;
	}
    }
    else {
	::CancelIo(hd);
	if (0 == ::CloseHandle(hd)) {
#ifdef qDebug
	    qDebug() << "Windows: SerialDeviceEnumeratorPrivate::isBusy() \n"
			" -> function: ::CloseHandle(hd) returned 0. Error! \n";
#endif
	}
    }
    return ret;
}

static Variant getDeviceRegistryProperty(::HDEVINFO DeviceInfoSet,
					 ::PSP_DEVINFO_DATA DeviceInfoData,
					 ::DWORD property)
{
    ::DWORD dataType = 0;
    ::DWORD dataSize = 0;
    Variant v;

    ::SetupDiGetDeviceRegistryProperty(DeviceInfoSet,
				       DeviceInfoData,
				       property,
				       &dataType,
				       0,
				       0,
				       &dataSize);

    std::vector<BYTE> data(dataSize);

    if (::SetupDiGetDeviceRegistryProperty(DeviceInfoSet,
					   DeviceInfoData,
					   property,
					   0,
					   reinterpret_cast<unsigned char*>(data.data()),
					   dataSize,
					   0)) {

	switch (dataType) {

	case REG_EXPAND_SZ:
	case REG_SZ: {
	    String s;
	    if (dataSize) {
		s = String((const char*)data.data());
	    }
	    v = Variant(s);
	    break;
	}

	case REG_MULTI_SZ: {
	    StringList l;
	    if (dataSize) {
		int i = 0;
		for (;;) {
		    String s((const char *)data.data() + i);
		    i += s.length() + 1;

		    if (s.empty())
			break;
		    l.append(s);
		}
	    }
	    v = Variant(l);
	    break;
	}

	case REG_DWORD_BIG_ENDIAN:
	case REG_DWORD: {
	    assert(data.size() == sizeof(int));
	    int i = 0;
	    ::memcpy((void *)(&i), data.data(), sizeof(int));
	    v = i;
	    break;
	}

	default:
	    v = Variant();
	}

    }

    return v;
}


static const String RegExpContent("V[iI][dD]_(\\w+)&P[iI][dD]_(\\w+)");


//get Vendor ID from HardwareID
static String getNativeVendorID(const StringList &hardvareID)
{
    String s = hardvareID.join(";");

    if (hardvareID.empty())
	return String();

#ifdef USE_C11
    std::regex re(RegExpContent);
    s = hardvareID.join(";");

    std::smatch match;
    if (std::regex_search(s, match, re) && match.size() > 1)
	s = match.str(1);
    else
	s.clear();
#else
    size_t pos = s.find("Vid_");
    if (pos != std::string::npos)
	s = String(s.c_str() + pos + 4, 4);
    else
    {
	pos = s.find("VID_");
	if (pos != std::string::npos)
	    s = String(s.c_str() + pos + 4, 4);
	else
	    s.clear();
    }
#endif
    return s;
}

//get Product ID from HardwareID
static String getNativeProductID(const StringList &hardvareID)
{
    String s = hardvareID.join(";");

    if (hardvareID.empty())
	return String();

#ifdef USE_C11
    std::regex re(RegExpContent);

    std::smatch match;
    if (std::regex_search(s, match, re) && match.size() > 2)
	s = match.str(2);
    else
	s.clear();
#else
    size_t pos = s.find("Pid_");
    if (pos != std::string::npos)
	s = String(s.c_str() + pos + 4, 4);
    else
    {
	pos = s.find("PID_");
	if (pos != std::string::npos)
	    s = String(s.c_str() + pos + 4, 4);
	else
	    s.clear();
    }
#endif

    return s;
}

//get name of device
static String getNativeName(::HDEVINFO DeviceInfoSet,
			    ::PSP_DEVINFO_DATA DeviceInfoData) {

    ::HKEY key = ::SetupDiOpenDevRegKey(DeviceInfoSet,
					DeviceInfoData,
					DICS_FLAG_GLOBAL,
					0,
					DIREG_DEV,
					KEY_READ);

    String result;

    if (INVALID_HANDLE_VALUE == key)
	return result;

    ::LONG ret = ERROR_SUCCESS;
    ::DWORD i = 0;
    ::DWORD keyType = 0;
    std::vector<BYTE> buffKeyName(16384);
    std::vector<BYTE> buffKeyVal(16384);
    for (;;) {
	::DWORD lenKeyName = buffKeyName.size();
	::DWORD lenKeyValue = buffKeyVal.size();

	ret = ::RegEnumValue(key,
			     i++,
			     reinterpret_cast<LPSTR>(buffKeyName.data()),
			     &lenKeyName,
			     0,
			     &keyType,
			     reinterpret_cast<unsigned char*>(buffKeyVal.data()),
			     &lenKeyValue);

	if (ERROR_SUCCESS == ret) {
	    if (REG_SZ == keyType) {

		String itemName((const char* )buffKeyName.data(), lenKeyName);
		String itemValue((const char* )buffKeyVal.data(), lenKeyValue);

		if (itemName.contains("PortName")) {
		    result = itemValue;
		    break;
		}
	    }
	}
	else
	    break;
    }

    ::RegCloseKey(key);

    return result;
}

//get device driver name
static String getNativeDriver(const String &service)
{
    String result;
    String subKey("SYSTEM\\CurrentControlSet\\services\\");

    subKey.append(service);

    ::HKEY key;
    ::LONG res = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
				subKey.c_str(),
				0,
				KEY_QUERY_VALUE,
				&key);

    if (ERROR_SUCCESS == res) {

	DWORD dataType = 0;
	DWORD dataSize = 0;

	subKey = "ImagePath";
	res = ::RegQueryValueEx(key,
				subKey.c_str(),
				0,
				&dataType,
				0,
				&dataSize);

	if (ERROR_SUCCESS == res) {
	    std::vector<BYTE> data(dataSize);
	    res = ::RegQueryValueEx(key,
				    subKey.c_str(),
				    0,
				    0,
				    data.data(),
				    &dataSize);

	    if (ERROR_SUCCESS == res) {

		switch (dataType) {
		case REG_EXPAND_SZ:
		case REG_SZ: {
		    if (dataSize) {
			//result = String::fromWCharArray(((const wchar_t *)data.data()));

		    }
		    break;
		}
		default:;
		}
	    }
	}
	::RegCloseKey(key);
    }
    return result;
}

static const ::GUID guidArray[] =
{
    /* Windows Ports Class GUID */
    { 0x4D36E978, 0xE325, 0x11CE, { 0xBF, 0xC1, 0x08, 0x00, 0x2B, 0xE1, 0x03, 0x18 } },
    /* Virtual Ports Class GUIG (i.e. com0com, nmea and etc) */
    { 0xDF799E12, 0x3C56, 0x421B, { 0xB2, 0x98, 0xB6, 0xD3, 0x64, 0x2B, 0xC8, 0x78 } },
    /* Windows Modems Class GUID */
    { 0x4D36E96D, 0xE325, 0x11CE, { 0xBF, 0xC1, 0x08, 0x00, 0x2B, 0xE1, 0x03, 0x18 } }
};

void SerialDeviceEnumeratorPrivate::updateInfo()
{
    SerialInfoMap info;

    static int guidCount = sizeof(guidArray)/sizeof(::GUID);
    for (int i = 0; i < guidCount; ++i) {

	::HDEVINFO DeviceInfoSet = ::SetupDiGetClassDevs(&guidArray[i],
							 0,
							 0,
							 DIGCF_PRESENT);

	if (INVALID_HANDLE_VALUE == DeviceInfoSet) {
#ifdef qDebug
	    qDebug() << "Windows: SerialDeviceEnumeratorPrivate::updateInfo() \n"
			" -> ::SetupDiGetClassDevs() returned INVALID_HANDLE_VALUE, \n"
			" -> last error: "
		     << ::GetLastError() << ". Error! \n";
#endif
	    infoMap = info;
	    return;
	}

	::SP_DEVINFO_DATA DeviceInfoData = {0};
	//::memset(&DeviceInfoData, 0, sizeof(::SP_DEVINFO_DATA));
	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

	::DWORD DeviceIndex = 0;
	while (::SetupDiEnumDeviceInfo(DeviceInfoSet,
				       DeviceIndex++,
				       &DeviceInfoData)) {

	    SerialInfo si;
	    Variant v;

	    //get device name
	    v = getNativeName(DeviceInfoSet, &DeviceInfoData);
	    String s = v.toString();

	    if ((!s.empty()) && (!s.contains("LPT"))) {
		//bus
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_ENUMERATOR_NAME);
		si.bus = v.toString();
		//description
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_DEVICEDESC);
		si.description = v.toString();
		//friendly name
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_FRIENDLYNAME);
		si.friendlyName = v.toString();
		//hardware ID
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_HARDWAREID);
		si.hardwareID = v.toStringlist();
		//location info
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_LOCATION_INFORMATION);
		si.locationInfo = v.toString();
		//manufacturer
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_MFG);
		si.manufacturer = v.toString();
		//sub system
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_CLASS);
		si.subSystem = v.toString();
		//service
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_SERVICE);
		si.service = v.toString();
		//driver
		v = getNativeDriver(si.service);
		si.driverName = v.toString();
		//system path
		v = getDeviceRegistryProperty(DeviceInfoSet, &DeviceInfoData, SPDRP_PHYSICAL_DEVICE_OBJECT_NAME);
		si.systemPath = v.toString();
		//product ID
		si.productID = getNativeProductID(si.hardwareID);
		//vendor ID
		si.vendorID = getNativeVendorID(si.hardwareID);
		//short name
		si.shortName = s;

		//add to map
		info[s] = si;
	    }
	}

	::SetupDiDestroyDeviceInfoList(DeviceInfoSet);
    }
    infoMap = info;
}

bool SerialDeviceEnumeratorPrivate::isValid() const
{
    return (this->eHandle && this->keyHandle);
}
