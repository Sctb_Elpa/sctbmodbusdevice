/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef MBCELL_H
#define MBCELL_H

#include <cstdint>
#include <typeinfo>
#include <typeindex>

#include <SCTBModbusDevice.h>

namespace SCTBModbusDevice
{

class Device;

/**
 * @class AbstractCell
 * Базовый клас для ячеек modbus всех типов
 */
class SCTB_MODBUS_DEVICE_EXPORT AbstractCell
{
public:
    /// Тип ячейки
    enum enType
    {
        /// Не инициализирован/указан
        INVALID = 0,
        /// Holding Register
        HoldingRegister = 1,
        /// Input Register
        InputRegister = 2,
        /// Coil
        Coil = 3,
        /// Discrete Input
        DiscreteInput = 4,

        _TypeEnd = 6
    };

    /** Конструктор
     * @param parent Устройство, которому принадлежит ячейка
     * @serialisable Флаг запрета группового обращения\n
     *      true - групповой обмен разрешен
     *      false - Групповой обмен запрещен
     */
    AbstractCell(Device* parent = NULL, bool serialisable = true) :
        mValType(NULL)
    {
        setParent(parent);
        mCacheIsValid = false;
        mSerialisable = serialisable;
        mReadonly = false;
        mType = INVALID;

        createtypesizemap();
    }

    /// Деструктор
    virtual ~AbstractCell() {}

    /**
     * @brief Клонировать ячейку
     *
     * Создает копию ячейки и всех ей полей
     * @return Указатель на клона
     */
    virtual AbstractCell *clone() const = 0;

    /**
     * @brief попытка кешировать значения ячейки для быстрого чтения
     * @param verbose отобразить в консоли сообщение в случае неудачи
     * @return
     *  true - операция успешно завершена\n
     *  false - Операция завершилась с ошибкой
     */
    virtual bool cache(bool verbose = true) = 0;

    /**
     * @brief Получить размер значения ячейки в байтах
     * @return Количество байт, занимаемое значением ячейки
     */
    virtual unsigned int valueSize() const = 0;

    /**
     * @brief Получить адрес ячейки
     * @return Адрес ячейки
     */
    uint16_t Address() const { return mAddress; }

    /**
     * @brief Установить адрес ячейки
     * @param address новый адрес ячейки
     */
    virtual void setAddress(uint16_t address) { mAddress = address; }

    /**
     * @brief Тип значения, содержащегося в ячейке
     * @return std::type_index, соотвстствующий типу значения в ячейке
     */
    const std::type_index& ValueType() const { return *mValType; }

    virtual void setValue();

    /**
     * @brief Получить тип ячейки
     * @return Тип ячейки
     */
    enType Type() const { return mType; }

    /**
     * @brief Установить тип ячейки
     * @param type тип ячейки
     */
    virtual void setType(enType type) { mType = type; }

    /**
     * @brief Получить флаг группового доступа ячейки
     * @return
     *  true - Доступны операции с ячейкой в группе\n
     *  false - только индивидуальный доступ
     */
    bool Serialisable() const { return mSerialisable; }

    /**
     * @brief Установить флаг группового доступа ячейки
     * @param serialisable \n
     *  true - Доступны операции с ячейкой в группе\n
     *  false - только индивидуальный доступ
     */
    virtual void setSerialisable(bool serialisable)
    { mSerialisable = serialisable; }

    /**
     * @brief Получить устройство, которому принадлежит ячейка
     * @return Указатель на устройство, которому принадлежит ячейка
     */
    Device* Parent() const { return mParent; }

    /**
     * @brief Принудительно очистить кеш
     */
    virtual void invalidateCache() { mCacheIsValid = false; }

    /**
     * @brief Проверить валидность кеша
     * @return
     *  true - кеш валиден\n
     *  false - кеш просрочен
     */
    virtual bool cacheIsValid() const { return mCacheIsValid; }

    /**
     * @brief Установить родителя
     * @param parent Устройство, которому принадлежит ячейка
     */
    virtual void setParent(Device* parent) { mParent = parent; }

    /**
     * @brief Получить значение флага "защит записи"
     * @return
     *  true - Запись запрещена\n
     *  false - Запись разрешена
     */
    bool IsReadonly() const { return mReadonly; }

    /**
     * @brief Установить защиту записи
     * @param readonly \n
     *  true - Запись запрещена\n
     *  false - Запись разрешена
     */
    void setReadOnly(bool readonly) { mReadonly = readonly; }

    /**
     * @brief Получить имя ячейки
     * @return Имя ячейки
     */
    String Name() const { return mName; }

    /**
     * @brief Установить имя ячейки
     * @param name имя ячейки
     */
    virtual void setName(const String &name) { mName = name; }

    /**
     * @brief Получить категорию, к которой относится ячейка
     * @return Название категории
     */
    String Category() const { return mCategory; }

    /**
     * @brief Установить категорию, к которой относится ячейка
     * @param category Название категории
     */
    virtual void setCategory(const String &category) { mCategory = category; }

    /**
     * @brief Получить описание назначения ячейки
     * @return Описание ячейки
     */
    String Description() const { return mDescription; }

    /**
     * @brief Установить описание назначения ячейки
     * @param desc Описание ячейки
     */
    virtual void setDescription(const String &desc) { mDescription = desc; }

    /**
     * @brief Установить значение ячейки из сырых данных
     * @param d Указатель на сырые данные
     */
    virtual void fromRaw(const void* d) {}

    /**
     * @brief Записать значение ячейки в указанное место
     * @param pos Указатель на место в памяти, куда будет записано значение
     * ячейки
     * @return Длина записанного в байтах
     */
    virtual size_t writeraw(void* pos) { return 0; }

    /**
     * @brief Формирует строку, содержащую информацию о ячейке
     * @return Строка, содержащая описание ячейки
     */
    virtual String toString() const;

    /**
     * @brief Формирует строку, содержащую значение ячейки
     * @return Строка, соответствующая значению ячейки
     */
    virtual String valString() = 0;

    /**
     * @brief Сравнивает две ячейки по типу и адресу (для сортировки)
     * @param enother правый аргумент у знака < (Другая ячейка)
     */
    virtual bool operator<(const AbstractCell& enother) const;

    /**
     * @brief проверяет ячейки на однотипность по адресу, типу и
     * типу значения (для поиска дубликатов)
     * @param enother правый аргумент у знака < (Другая ячейка)
     * @return
     *  true - Ячейки идентичны\n
     *  false - Ячейки различны
     */
    virtual bool operator==(const AbstractCell& enother) const;

protected:
    uint16_t mAddress;
    bool mSerialisable;

    Device* mParent;
    std::type_index *mValType;
    enType mType;

    bool mCacheIsValid;
    bool mReadonly;

    String mName, mCategory, mDescription;

    static void createtypesizemap();

    static int typeInfoToSize(const std::type_index& info);
};

}

#endif // MBCELL_H
