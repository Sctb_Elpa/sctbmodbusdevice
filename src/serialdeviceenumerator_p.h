/*
* This file is part of QSerialDevice, an open-source cross-platform library
* Copyright (C) 2009  Denis Shienkov
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Denis Shienkov:
*          e-mail: <scapig2@yandex.ru>
*             ICQ: 321789831
*/

#ifndef SERIALDEVICEENUMERATOR_P_H
#define SERIALDEVICEENUMERATOR_P_H

#include <map>

#ifdef _WIN32
#include <windows.h>
#endif

#include "SerialDeviceEnumerator.h"
#include "SCTBModbusDevice.h"

namespace SCTBModbusDevice {

/*
  Inner class for storing information received on a one serial device.
  Fields of class are some options.
*/
class SerialInfo
{
public:
    String shortName;
    String systemPath;
    String subSystem;
    String locationInfo;
    String driverName;
    String friendlyName;
    String description;
    StringList hardwareID;
    String vendorID;
    String productID;
    String manufacturer;
    String service;
    String bus;
    String revision;
    //
	inline bool operator==(const SerialInfo &c) const { return !(*this != c); }

    inline bool operator!=(const SerialInfo &c) const
    {
        return (!((this->bus == c.bus)
                && (this->description == c.description)
                && (this->driverName == c.driverName)
                && (this->friendlyName == c.friendlyName)
                && (this->hardwareID == c.hardwareID)
                && (this->locationInfo == c.locationInfo)
                && (this->manufacturer == c.manufacturer)
                && (this->productID == c.productID)
                && (this->revision == c.revision)
                && (this->service == c.service)
                && (this->shortName == c.shortName)
                && (this->subSystem == c.subSystem)
                && (this->systemPath == c.systemPath)
                && (this->vendorID == c.vendorID)));
    }

	inline bool operator<(const SerialInfo &c) const
	{
		if (this->vendorID == c.vendorID)
			return this->productID < c.productID;
		else
			return this->vendorID < c.vendorID;
	}
};

/*
  Inner class for storing information received from all found the serial devices.
  The class inherits from QMap, where as a key (QString), use the name of the
  serial devices as well as the value of the class SerialInfo.
*/
class SerialInfoMap : public Map<String, SerialInfo>
{
public:
    inline bool operator!=(const SerialInfoMap &m) const
    {
        unsigned int size = this->size();
        if ((m.size() != size)
            || (this->keys() != m.keys())) {
            return true;
        }

        List<SerialInfo> l1 = this->values();
        List<SerialInfo> l2 = m.values();

        while (size--) {
            if (l1.at(size) != l2.at(size))
                return true;
        }
        return false;
    }
};

class SerialDeviceEnumeratorPrivate
{
    friend class SerialDeviceEnumerator;
public:
            SerialDeviceEnumeratorPrivate();
    virtual ~SerialDeviceEnumeratorPrivate();

    void setNativeDeviceName(const String &name);
    String nativeName() const;
    String nativeShortName() const;
    String nativeSystemPath() const;
    String nativeSubSystem() const;
    String nativeLocationInfo() const;
    String nativeDriver() const;
    String nativeFriendlyName() const;
    String nativeDescription() const;
    StringList nativeHardwareID() const;
    uint16_t nativeVendorID() const;
    uint16_t nativeProductID() const;
    String nativeManufacturer() const;
    String nativeService() const;
    //
    String nativeBus() const;
    String nativeRevision() const;

    bool nativeIsExists() const;
    bool nativeIsBusy() const;

    SerialDeviceEnumerator * q_ptr;

private:
    SerialInfoMap infoMap; /* It stores information about all found devices with serial interface. */
    void updateInfo();
    String currName; /* It contains the current name of the serial device about which receives the information.
                        (The name set the setNativeDeviceName()). */
    SerialInfo currInfo; /* It contains the current info of the serial device about which receives the information.
                        (The name set the setNativeDeviceName()). */
#if defined (_WIN32)
    ::HANDLE eHandle;
    ::HKEY keyHandle;
#elif defined (__unix)
    struct udev *udev;
    int udev_socket;
    struct udev_monitor *udev_monitor;
    Map<String, String> eqBusDrvMap; /* It contains the line name bus device and its driver.
                                            Completed manually by the programmer. */
    StringList devNamesMask; /* Contains a list of masks device names for which there is filtration.
                                 Completed manually by the programmer. */
#endif
    bool isValid() const;
};

}

#endif // SERIALDEVICEENUMERATOR_P_H
