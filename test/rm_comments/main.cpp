/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

//http://www.cyberforum.ru/cpp-beginners/thread360883.html

#include <fstream>
#include <iostream>
using namespace std;
string comment_erase(string str);

int main(int argc, char* argv[]){
    string filename, outname;

    if (argc < 2)
    {
        cout << "No input files!\nUse: " << argv[0]
                << "<file> [file] ..." << endl;
    }
    else
        for (int i = 1; i < argc; ++i)
        {
            filename = string(argv[i]); // имя открываемого файла
            outname = filename + ".nocoments"; // имя выходного файла

            // открываем файл любой из (.cpp, .h, .c, .hpp)
            fstream fp(filename, fstream::in);
            if(! fp.is_open())
                return 1;
            long    size;
            string  str;
            fp.seekg(0L, ios::end);  // перемещаем ф-указатель в конец файла
            size = fp.tellg();    // получаем кол-во байт файла
            fp.seekg(0L, ios::beg);  // устанавливаем ф-указатель в начало
            if(! size) {  // если файл не имеет байтов закрываем прогу
                fp.close();
                return 2;
            }
            char* buf = new char[size];  // выделяем буфер под все байты из файла
            if(buf == NULL) { //ошибка кучи или не хватка ОЗУ
                fp.close();
                return 3;
            }
            fp.read(buf, size); // читаем весь файл в буфер
            fp.clear();
            fp.close();

            buf[size - 1] = '\0';
            str.assign(buf);   // передаём буфер объекту string для последующего анализа текста
            delete[] buf;  // очищаем кучу
            buf = NULL;

            str = comment_erase(str); // удаляем комментария

            fp.open(outname, fstream::out); // создаём другой файл(вдруг что-то важное в оригинале удалим)
            if(! fp.is_open())
                return 4;
            fp.write(str.c_str(), str.length());// записываем в выходной файл
            fp.flush();
            fp.clear();
            fp.close();
        }
    return 0;
}

string comment_erase(string str) {
    int  id;
    string::iterator tmp, last;
    for(string::iterator  iter = str.begin(); iter != str.end(); *iter++) {
        if( *iter == '"') // в строках никогда комментария не ищем
            for(iter += 1; *iter != '"' && iter != str.end(); *iter++);
        
        if(*iter == '/') {  // встречается обратная черта, а вдруг это не деление а началo многострчного комментария
            if(*(iter + 1) == '/') { // и так это однострочный комментарий
                for(last = iter + 1; *last != '\n' && last != str.end(); *last++);
                tmp = iter - 1;
                str.erase(iter, last);  // удаляем диапaзон между N = last - iter
                iter = tmp;
            } else if( *(iter + 1) == '*') { // а может быть это многострочный комментарий
                id = 0;
                for(last = iter + 1; last + 1 != str.end(); *last++) {
                    if( *last == '*' && *(last + 1) == '/') {  // ищем конец многострочнго комментария
                        id = 1;
                        last += 2;
                        break;    // если конец найден прерываем цикл для удаления
                    }
                }
                if(id) {
                    // сохраним позицию итератора чтобы не бегать всё заново по тексту
                    tmp = iter - 1;
                    str.erase(iter, last);  // тоже самое удаление диапазона комментария
                    iter = tmp;  // продолжаем работу в поиске комментарий
                }
            }
        }
    }
    return str;
}
