cmake_minimum_required(VERSION 2.8)

project(cellquerry)

#список сырцов
set(${PROJECT_NAME}_SRCS
    main.cpp
    )

add_executable(${PROJECT_NAME}
    ${${PROJECT_NAME}_SRCS}
    )

# импортируется как знание где взять SCTBModbusDevice_static так и
# его инклюды
target_link_libraries(${PROJECT_NAME} SCTBModbusDevice_static)
