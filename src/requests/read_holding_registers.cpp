/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <cassert>
#include <cstring>
#include <stdexcept>

#include "modbus.h"

#include "Connection.h"

#include "read_holding_registers.h"

using namespace SCTBModbusDevice;

ReadHoldingRegisters::ReadHoldingRegisters(uint8_t SlaveAddress,
                                           uint16_t startAdress, uint8_t regCount):
    request(SlaveAddress, startAdress, regCount)
{
    m_status = READY;
}

void ReadHoldingRegisters::process(Connection *_io)
{
    assert(m_status != NOT_INITIALIZED);

    if (!Count())
    {
        ansver_data.clear();
        return;
    }

    if (_io->isBusy())
        m_status = WAITING;

    _io->lock();
    m_status = PROCESSING;

    uint16_t buff[0xff];
    int readed = modbus_read_registers(_io->modbusContext(),
                                       startAddress(), Count(), buff);
    if (readed > 0)
    {
        // success
        ansver_data.resize(readed * sizeof(uint16_t));
        memcpy(ansver_data.data(), buff, readed * sizeof(uint16_t));
    }
    else
    {
         // fail
        setError(errno);
        ansver_data.clear();
    }
}

