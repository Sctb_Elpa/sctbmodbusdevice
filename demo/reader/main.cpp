/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
 * Данный пример показывает опрос устройства при помощи отправки
 * запросов к нему.
 */

#include <stdexcept>
#include <iostream>

#include <Prober.h>
#include <Device.h>
#include <SerialDeviceEnumerator.h>
#include <requests/read_holding_registers.h>
#include <requests/read_input_registers.h>
#include <requests/read_coils.h>
#include <requests/read_discrete_inputs.h>

#define FT232_VID   0x0403 // Идентификатор производителя FTDI
#define FT232_PID   0x6001 // Идентификатор микросхемы FT232RL

#ifdef _WIN32
// MSVC has no unistd.h and usleep()
// http://stackoverflow.com/questions/5801813/c-usleep-is-obsolete-workarounds-for-windows-mingw
#include <windows.h>
void usleep(__int64 usec)
{ 
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10*usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
}
#else
#include <unistd.h>
#endif

using namespace SCTBModbusDevice;

// Колбек, отображающий прогресс сканирования шины
void informer(void* __unused, uint8_t progress)
{
    std::cout << '\r' << "Scaning: 0x" << std::hex << (int)progress;
    std::cout.flush();
}

int main(int argc, char* argv[])
{
    int exitcode = 0;

    // получаем экземпляр перечислителя
    SerialDeviceEnumerator *enumerator =
            SerialDeviceEnumerator::instance();

    //получаем список последовательных устройств в системе
    StringList avalable = enumerator->devicesAvailable();

    List<Device*> scanres; // результат поиска
    Connection *IO = NULL; // транспортное соединение

    // Обходим список последовательных устройств, чтобы обнаружить только
    // микросхемы FT232RL (преобразователь USB <-> RS232)
    StringList::const_iterator it = avalable.begin();
    for(;it != avalable.end(); ++it)
    {
        enumerator->setDeviceName(*it);

        if (enumerator->vendorID() == FT232_VID
                && enumerator->productID() == FT232_PID)
        {
            // микросхема FT232RL обнаружена

            if (IO)
                delete IO;

            // Создаем транспортное соединение на найденном м порту с
            // параметрами по-уполчанию
            IO = Connection::newRTU(*it);

            // Создаем экземпляр сканера на созданном ранее транспортном
            // соединении
            Prober *prober = new Prober(IO);

            // Регистрируем функцию обратного вызова
            prober->registerProgressInformer(informer, NULL);

            // Запускаем сканирование
            prober->start();
            // Ожидаем окончание сканирования
            prober->join();

            // получаем список найденных устройств
            scanres = prober->getProbedDevices();

            delete prober;

            // если хотябы одно устройство найдено, завершаем поиск
            if (scanres.size())
                break;
        }
    }

    // удаляем перчислитель
    SerialDeviceEnumerator::release();

    if (scanres.empty())
    {
        std::cout << std::endl << "No devices found" << std::endl;
        exitcode = 0;
    }
    else
    {
        std::cout << "Found " << scanres.size() << " Device(s)." << std::endl;

        // получаем адрес первого найденного устройства
        uint8_t Addr = scanres.front()->Address();

        // унечтожаем найденные устройства
        for(List<Device*>::const_iterator it = scanres.begin();
            it != scanres.end(); ++it)
            delete *it;


        // создаем запросы к устройству:

        // Запрос чтения первого регистра хранения
        ReadHoldingRegisters idReq(Addr /*, 0, 1*/);
        // Запрос чтения вторго регистра хранения
        ReadHoldingRegisters serialReq(Addr, 1);
        // Запрос чтения первых двух входных регистров
        ReadInputRegisters valReq(Addr, 0, 2);
        // Запрос чтения первой катушки
        ReadCoils coilsTest(Addr);
        // Запрос чтения первого дискретного входа
        ReadDiscreteInputs readErrorFlags(Addr);

        try
        {
            // выполняем запросы
            idReq.exec(IO);
            serialReq.exec(IO);
            coilsTest.exec(IO);

            if (idReq.error() == modbusException::OK &&
                    serialReq.error() == modbusException::OK)
                std::cout << std::endl << "First found device:" << std::endl
                          << "ID: 0x" << std::hex << idReq.toUInt16().at(0) << std::endl
                          << "Serial: " << std::dec << serialReq.toUInt16().at(0) << std::endl
                          << "Coil0: " << coilsTest.toBool().at(0) << std::endl;
            else
                std::cerr << "Error read holding registers" << std::endl;

        }
        catch (std::exception e)
        {
            // во время выполнения произошла ошибка
            std::cerr << "Error read holding registers: " << e.what() << std::endl;
            exitcode = -1;
            goto __fin;
        }

        int c = 10;
        while (c--)
        {
            try
            {
                // выполняем чтение входов
                valReq.exec(IO);
                readErrorFlags.exec(IO);
                if (valReq.error() == modbusException::OK)
                    std::cout << "Value: "
                                 /* Преобразовать 2 регистра к одному значению
                                    типа float */
                              << valReq.toFloat().at(0)
                              << "\tERR status: "
                              << readErrorFlags.toBool().at(0) << std::endl;
                else
                    std::cerr << "Read error: " << valReq.error().what() << std::endl;
            }
            catch (std::exception e)
            {
                // ошибка при выполнении запроса
                std::cerr << e.what() << std::endl;
                exitcode = -1;
                goto __fin;
            }

            usleep(100000); // пауза между запросами
        }
    }

__fin:
    delete IO;

    return exitcode;
}
