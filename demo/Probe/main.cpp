/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
 * Данный пример показывает работу с классом SCTBModbusDevice::Prober для поиска
 * устройств на последовательной шине modbus
 */

#include <stdexcept>
#include <iostream>

#include <Prober.h>
#include <SerialDeviceEnumerator.h>
#include <Device.h>

#define FT232_VID   0x0403 // Идентификатор производителя FTDI
#define FT232_PID   0x6001 // Идентификатор микросхемы FT232RL

// Колбек, отображающий прогресс сканирования шины
void informer(void* __unused, uint8_t progress)
{
    (void)__unused;
    std::cout << std::endl << "Scaning: 0x" << std::hex << (int)progress;
    std::cout.flush();
}

// Колбек, отображающий результат поиска
void displayRes(void* d)
{
    // d - указатель на экземпляр SCTBModbusDevice::Prober
    // получаем список обнаруженных устройств
    SCTBModbusDevice::List<SCTBModbusDevice::Device*> probeResult =
            static_cast<SCTBModbusDevice::Prober*>(d)->getProbedDevices();

    // Обходим список обнаруженных устройств, печатаем информацию о них
    for(SCTBModbusDevice::List<SCTBModbusDevice::Device*>::const_iterator mi =
        probeResult.begin(); mi != probeResult.end(); ++mi)
    {
        std::cout << std::endl
                  << "Found device ID = 0x" << std::hex << (*mi)->id()
                  << " at address = 0x"
                  << SCTBModbusDevice::String::number((int)(*mi)->Address())
                  << std::endl;
    }
}

int main(int argc, char* argv[])
{
    // получаем экземпляр перечислителя
    SCTBModbusDevice::SerialDeviceEnumerator *enumerator =
            SCTBModbusDevice::SerialDeviceEnumerator::instance();

    //получаем список последовательных устройств в системе
    SCTBModbusDevice::StringList avalable = enumerator->devicesAvailable();

    // Обходим список последовательных устройств, чтобы обнаружить только
    // микросхемы FT232RL (преобразователь USB <-> RS232)
    SCTBModbusDevice::StringList::const_iterator it = avalable.begin();
    for(;it != avalable.end(); ++it)
    {
        enumerator->setDeviceName(*it);

        if (enumerator->vendorID() == FT232_VID
                && enumerator->productID() == FT232_PID)
        {
            // микросхема FT232RL обнаружена

            // Создаем транспортное соединение на найденном м порту с
            // параметрами по-уполчанию
            SCTBModbusDevice::Connection *IO =
                    SCTBModbusDevice::Connection::newRTU(*it);

            // Создаем экземпляр сканера на созданном ранее транспортном
            // соединении
            SCTBModbusDevice::Prober *prober = new SCTBModbusDevice::Prober(IO);

            // Регистрируем функции обратного вызова
            prober->registerProgressInformer(informer, NULL);
            prober->registerFinishedCallback(displayRes, prober);

            // Запускаем сканирование
            prober->start();
            // Ожидаем окончание сканирования
            prober->join();

            delete IO;
            delete prober;
        }
    }

    SCTBModbusDevice::SerialDeviceEnumerator::release();

    return 0;
}
