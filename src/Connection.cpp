/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <stddef.h>
#include <stdexcept>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <cstdio>
#include <iostream>

#include "pthread.h"

#include "modbus.h"
#include "common.h"

#include "Connection.h"

using namespace SCTBModbusDevice;

Connection *Connection::newRTU(const std::string &device,
               int baud,
               char parity,
               int data_bit,
               int stop_bit
               )
{
    Connection *res = new Connection();

#if _WIN32
    String _device = device;
    if(memcmp("COM", _device.c_str(), 3) == 0) // portname starts from "COM"
        _device.insert(0, "\\\\.\\");

    res->mb = modbus_new_rtu(_device.data(), baud, parity, data_bit, stop_bit);
#else
    res->mb = modbus_new_rtu(device.data(), baud, parity, data_bit, stop_bit);
#endif
    if (!res->mb)
    {
        delete res;
        throw std::runtime_error(
                common::make_error_msg(
                    "Fauled to create libmodbus RTU instance: %s"));
    }
    return res;
}

Connection *Connection::newTCP(const std::string &ip, int port)
{
    Connection *res = new Connection();

    res->mb = modbus_new_tcp(ip.data(), port);
    if (!res->mb)
        throw std::runtime_error(
                common::make_error_msg(
                    "Fauled to create libmodbus TCP instance: %s"));

    return res;
}

Connection::~Connection()
{
    // быдл бы не плохо понять почему выдает EBUSY
    pthread_mutex_destroy(mutex);

#ifndef _WIN32
    free(owner);
#endif

    free(mutex);
    if (mb)
    {
        close();
        modbus_free(mb);
    }
}

void Connection::setIODebug(bool enabled)
{
    modbus_set_debug(mb, enabled);
}

void Connection::setResponseTimeout(Connection::timeval timeout)
{
    modbus_set_response_timeout(mb, timeout.tv_sec, timeout.tv_usec);
}

Connection::timeval Connection::responceTimeout() const
{
    timeval res;
    modbus_get_response_timeout(mb, &res.tv_sec, &res.tv_usec);
    return res;
}

void Connection::connect(uint8_t slaveAddr)
{
    checkOwnerThread();

    if (openedAddr == -1)
        if (modbus_connect(mb))
            throw std::runtime_error(common::make_error_msg(
                                         "Connection failed: %s"));
    if (openedAddr != slaveAddr)
        modbus_set_slave(mb, slaveAddr);

    openedAddr = slaveAddr;
}

void Connection::close()
{
    modbus_close(mb);
    openedAddr = -1;
}

void Connection::flush()
{
    modbus_flush(mb);
}

bool Connection::isBusy()
{
    if (pthread_mutex_trylock(mutex) == EINVAL)
        return true;
    else
    {
        pthread_mutex_unlock(mutex);
        return false;
    }
}

bool Connection::tryLock()
{
    if (!pthread_mutex_trylock(mutex))
    {
        setOwner();
        return true;
    }
    return false;
}

void Connection::lock()
{
    pthread_mutex_lock(mutex);
    setOwner();
}

void Connection::unlock()
{
    checkOwnerThread();

    pthread_mutex_unlock(mutex);
    clearOwner();
}

modbus_t *Connection::modbusContext() const
{
    checkOwnerThread();
    assert(openedAddr != -1);
    return mb;
}

Connection::Connection()
{
    mb = NULL;
    openedAddr = -1;
#if _WIN32
    owner = NULL;
#else
    owner = static_cast<pthread_t*>(malloc(sizeof(pthread_t)));
#endif

    mutex = static_cast<pthread_mutex_t*>(malloc(sizeof(pthread_mutex_t)));
    if (pthread_mutex_init(mutex, NULL))
        throw std::runtime_error("Failed to init mutex");
}

void Connection::checkOwnerThread() const
{
#if _WIN32
    if (owner != GetCurrentThread())
#else
    pthread_t current_thread =  pthread_self();
    if (memcmp(owner, &current_thread, sizeof(pthread_t)))
#endif
        throw std::logic_error("Interface not locked by this thread");
}

void Connection::setOwner()
{
#if _WIN32
    owner = GetCurrentThread();
#else
    *owner = pthread_self();
#endif
}

void Connection::clearOwner()
{
#ifdef _WIN32
    owner = NULL;
#else
    memset(owner, 0, sizeof(pthread_t));
#endif
}
