
#ifndef _VARIANT_H_
#define _VARIANT_H_

#include <vector>

#include "SCTBModbusDevice.h"

namespace SCTBModbusDevice {
	class Variant
	{
	public:
		enum enValType
		{
			NONE,
			STRING,
			STRINGLIST,
			INT,
			BOOL
		};

		Variant(); 
		Variant(const Variant& pattern);
		Variant(const String& str);
		Variant(const StringList& sl);
		Variant(int val);
		Variant(bool val);
		~Variant();

		String toString() const;
		StringList toStringlist() const;

	private:
		std::vector<char> storage;
		enValType valType;
	};
}

#endif /* _VARIANT_H_ */