/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

/**
 * Данный пример показывает изменение настроек устройства на примере смены его
 * адреса на шине modbus на +1 и обратно
 * Устройство должно поддерживать данную процедуру
 */

#include <stdexcept>
#include <iostream>

#include <Prober.h>
#include <Device.h>
#include <SerialDeviceEnumerator.h>
#include <DeviceFactory.h>
#include <CellQuery.h>
#include <request.h>

#define FT232_VID   0x0403 // Идентификатор производителя FTDI
#define FT232_PID   0x6001 // Идентификатор микросхемы FT232RL

#ifdef _WIN32
// MSVC has no unistd.h and usleep()
// http://stackoverflow.com/questions/5801813/c-usleep-is-obsolete-workarounds-for-windows-mingw
#include <windows.h>
void usleep(__int64 usec)
{
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10*usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
}
#else
#include <unistd.h>
#endif

using namespace SCTBModbusDevice;

// Колбек, отображающий прогресс сканирования шины
void informer(void* __unused, uint8_t progress)
{
    std::cout << '\r' << "Scaning: 0x" << std::hex << (int)progress;
    std::cout.flush();
}

int main(int argc, char* argv[])
{
    int exitcode = 0;

    // получаем экземпляр перечислителя
    SerialDeviceEnumerator *enumerator =
            SerialDeviceEnumerator::instance();

    //получаем список последовательных устройств в системе
    StringList avalable = enumerator->devicesAvailable();

    List<Device*> scanres; // результат поиска
    Connection *IO = NULL; // транспортное соединение

    // Обходим список последовательных устройств, чтобы обнаружить только
    // микросхемы FT232RL (преобразователь USB <-> RS232)
    StringList::const_iterator it = avalable.begin();
    for(;it != avalable.end(); ++it)
    {
        enumerator->setDeviceName(*it);

        if (enumerator->vendorID() == FT232_VID
                && enumerator->productID() == FT232_PID)
        {
            // микросхема FT232RL обнаружена

            if (IO)
                delete IO;

            // Создаем транспортное соединение на найденном м порту с
            // параметрами по-уполчанию
            IO = Connection::newRTU(*it);

            // Создаем экземпляр сканера на созданном ранее транспортном
            // соединении
            Prober *prober = new Prober(IO);

            // Регистрируем функцию обратного вызова
            prober->registerProgressInformer(informer, NULL);

            // Запускаем сканирование
            prober->start();
            // Ожидаем окончание сканирования
            prober->join();

            // получаем список найденных устройств
            scanres = prober->getProbedDevices();

            delete prober;

            // если хотябы одно устройство найдено, завершаем поиск
            if (scanres.size())
                break;
        }
    }

    // удаляем перчислитель
    SCTBModbusDevice::SerialDeviceEnumerator::release();

    std::cout << std::endl;

    if (scanres.empty())
    {
        std::cout << std::endl << "No devices found" << std::endl;
        exitcode = 0;
    }
    else
    {
        std::cout << "Found " << scanres.size() << " Device(s)." << std::endl;

        // формируем список занятых на шине адресов по найденным устройствам
        std::set<uint8_t> Alive;
        for(List<Device*>::const_iterator it = scanres.begin();
            it != scanres.end(); ++it)
            Alive.insert((*it)->Address());

        for(List<Device*>::const_iterator it = scanres.begin();
            it != scanres.end(); ++it)
        {
            uint8_t oldAddr = (*it)->Address(); // страрый адрес
            std::cout << "Trying to change addres of device 0x"
                      << std::hex << (int)oldAddr << " : "
                      << (*it)->Class() << " "
                      << (*it)->Description() << std::endl;


            // новый адрес = старый + 1
            // проверяем, не занят ли он на шине
            uint8_t newAddr = oldAddr;

            while (Alive.find(newAddr) != Alive.end())
            {
                ++newAddr;
                if (newAddr == 0xff)
                {
                    std::cout << ">>>Skip.";
                    continue;
                }
            }

            try
            {
                // пытаемя сменитьт адрес устройсва
                std::cout << "Changing\t0x" << (int)oldAddr << "->0x"
                          << (int)newAddr << "\t";

                (*it)->changeAddress(newAddr);

                std::cout << "Success!" << std::endl;;
            }
            catch (modbusException e)
            {
                // операция завершена с ошибкой
                std::cout << "Fail! (" << e.what() << ")"
                          << std::endl;
                goto __fin;
            }

            std::cout << "Delay 1.5 sec" << std::endl;
            usleep(1500000);

            try
            {
                // обратная смена адреса
                std::cout << "Revert\t0x" << (int)newAddr << "->0x"
                          << (int)oldAddr << "\t";

                (*it)->changeAddress(oldAddr);

                std::cout << "Success!" << std::endl;;
            }
            catch (modbusException e)
            {
                // обратная смена завершилась с ошибкой
                std::cout << "Fail! (" << e.what() << ")"
                          << std::endl;
                goto __fin;
            }
        }
    }

__fin:
    // Удаляем найденные устройства
    for(List<Device*>::const_iterator it = scanres.begin();
        it != scanres.end(); ++it)
        delete *it;

    delete IO;

    return exitcode;
}
