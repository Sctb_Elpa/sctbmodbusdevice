/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef REQUEST_H
#define REQUEST_H

#include <cassert>
#include <stdint.h>
#include <vector>
#include <stdexcept>
#include <typeinfo>
#include <cstring>

#include <SCTBModbusDevice.h>
#include <Connection.h>

namespace SCTBModbusDevice {

/** @class modbusException
 *Исключение, содержащее информацию об ошибке modbus
 */
class SCTB_MODBUS_DEVICE_EXPORT modbusException : public std::runtime_error
{
public:
    /// Коды ошибок modbus
    enum enErrorCode
    {
        /// Успешно
        OK = 0,
        /// Неподдерживаемая функция
        ILLEGAL_FUNCTION = 1,
        /// Неверный адрес
        ILLEGAL_DATA_ADDRESS,
        /// Неверное значение
        ILLEGAL_DATA_VALUE,
        /// Ошибка выполнения команды
        SLAVE_OR_SERVER_FAILURE,
        /// ACK
        ACKNOWLEDGE,
        /// Устройство занято
        SLAVE_OR_SERVER_BUSY,
        /// NACK
        NEGATIVE_ACKNOWLEDGE,
        /// Ошибка четности
        MEMORY_PARITY,
        /// Неизвестная ошибка
        NOT_DEFINED,
        GATEWAY_PATH,
        GATEWAY_TARGET,

        /// Нет ответа
        NO_ANSVER,
        /// Неизвестная ошибка
        UNDEFINED
    };

    //@{
    /// @name Конструкторы

    modbusException(enErrorCode code = OK);
    modbusException(const modbusException& ref);
    modbusException(int code);

    //@}

    /** Оператор проверки соответствия коду ошибки
     *  @param code код ошибки
     *  @return
     *      true - Код ошибки соответствует выброшенному исключению
     *      false - Исключение выброшено с другой ошибкой
     */
    bool operator==(enErrorCode code) { return err == code; }

    /** Оператор проверки несоответствия коду ошибки
     *  (аналогично operator==)
     */
    bool operator!=(enErrorCode code) { return err != code; }

    /// Оператор проверки равноценности исключений по коду ошибки
    bool operator==(const modbusException& exp) { return err == exp.err; }

    /** Получить код ошибки, по которому было выброшено исключение
     *  @return
     *      Код ошибки
     */
    enErrorCode code() const { return err; }

private:
    enErrorCode err;
};

class request;

namespace convertHalpers {
template<typename resType> inline std::vector<resType> toType(const request& r);
template<> std::vector<bool> toType<bool>(const request &r);
template<> std::vector<int16_t> toType<int16_t>(const request &r);
template<> std::vector<int32_t> toType<int32_t>(const request &r);
template<> std::vector<uint16_t> toType<uint16_t>(const request &r);
template<> std::vector<uint32_t> toType<uint32_t>(const request &r);
template<> std::vector<float> toType<float>(const request &r);
template<> std::vector<double> toType<double>(const request &r);
}

/**
 * @Class request
 * Базовый класс для всех типов запросов по modbus
 */
class SCTB_MODBUS_DEVICE_EXPORT request
{
public:
    /// Статус обработки запроса
    enum enStatus
    {
        /// Не инициализирован
        NOT_INITIALIZED,
        /// Готов
        READY,
        /// Ожидает в очереди
        WAITING,
        /// Выполняется операция ввода/вывода
        PROCESSING,
        /// Выполнение завершено
        FINISHED
    };

    /** Конструктор объекта-запроса
     *  @param slaveAddr Адрес устройства, которому предназначен запрос
     *  @param startAddr Адрес стартовой ячейки
     *  @param count Количество ячеек (как минимум 1)
     */
    request(uint8_t slaveAddr = 0, uint16_t startAddr = 0, uint8_t count = 1);

    /// Деструктор
    virtual ~request();

    /** Измениять адрес устройства, к которому преднозначен запрос
     *  @param slaveAddr Адрес устройства, которому предназначен запрос
     */
    virtual void setSlave(uint8_t slaveAddr);

    /** Получить адрес устройства, к которому преднозначен запрос
     *  @return Адрес устройства, которому предназначен запрос
     */
    virtual uint8_t slaveAddress() const { return slaveAddr; }

    /** Измениять адрес стартовой ячейки
     *  @param slaveAddr Адрес стартовой ячейки
     */
    virtual void setStartAdress(uint16_t startAddr);

    /** Получить адрес стартовой ячейки
     *  @return Адрес стартовой ячейки
     */
    virtual uint16_t startAddress() const { return startAddr; }

    /** Изменить количество ячеек, затрагиваемое запросм
     *  @param count установить количество ячеек, затрагиваемое запросм
     */
    void setCount(uint8_t count);

    /** Получить количество ячеек, затрагиваемое запросм
     *  @return количество ячеек, затрагиваемое запросм
     */
    uint8_t Count() const { return count; }

    /** Установить режим использования контрольной суммы
     *  @param useCRC
     *      true - Используется проверка целостности по CRC16 (default)
     *      false - Поле CRC16 не добавляется к посылке, проверка отключена
     */
    virtual void setUseCRC(bool useCRC);

    /** Проверка использования CRC в данном запросе
     *  @return
     *      true - Используется проверка целостности по CRC16 (default)
     *      false - Поле CRC16 не добавляется к посылке, проверка отключена
     */
    virtual bool IsUseCRC() const { return useCRC != 0; }

    //@{
    /// @name Хелперы

    /** Преобразовать результат в вектор булевых значений
     *  @return результат выполнения запроса как вектор булевых значений
     *  Используется с ячейками типа Coil и Discrete Input
     */
    virtual std::vector<bool> toBool() const;

    /** Преобразовать результат в вектор значений типа uint16_t
     *  @return результат выполнения запроса как вектор значений типа uint16_t
     *  Операция невозможна для ячеек Coil и Discrete Input
     */
    virtual std::vector<uint16_t> toUInt16() const;

    /** Преобразовать результат в вектор значений типа uint32_t
     *  @return результат выполнения запроса как вектор значений типа uint32_t
     *  Операция невозможна для ячеек Coil и Discrete Input
     *  Операция невозможна при чтении нечетного количества ячеек
     */
    virtual std::vector<uint32_t> toUInt32() const;

    /** Преобразовать результат в вектор значений с плавающей точкой
     *  @return результат выполнения запроса как вектор значений типа
     *  с плавающей точкой
     *  Операция невозможна для ячеек Coil и Discrete Input
     *  Операция невозможна при чтении нечетного количества ячеек
     */
    virtual std::vector<float> toFloat() const;

    /** Преобразовать результат в вектор значений с плавающей точкой
     *  двойной точности
     *  @return результат выполнения запроса как вектор значений типа
     *  с плавающей точкой двойной точности
     *  Операция невозможна для ячеек Coil и Discrete Input
     *  Операция доступна при чтении количества ячеек кратного 4
     */
    virtual std::vector<double> toDouble() const;

    /** Шаблонный метод для представления результата, как вестора каких-либо
     *  значений
     *  @param resType Доступны следующие типы
     *  \n
     *  <table>
     *  <tr><th>bool
     *  <tr><th>int16_t
     *  <tr><th>uint16_t
     *  <tr><th>int32_t
     *  <tr><th>uint32_t
     *  <tr><th>float
     *  <tr><th>double
     *  </table>
     */
    template<typename resType>
    std::vector<resType> toType() const
    {
        return convertHalpers::toType<resType>(*this);
    }

    //@}

    /** Синхронно выполнить запрос
     *  @param _io Инициализированное транспортное соединение
     */
    void exec(Connection* _io);

    /** Получить текущий стратус запроса
     *  @return текущий статус запроса
     */
    virtual enStatus status() const { return m_status; }

    /** Получить информацию о последней ошибке
     *  @return объект-исключение, связанное с последней ошибкой,
     *  произошедшей во время выполнения данного запроса
     */
    virtual modbusException error() const { return m_error; }

    /** Получить "сырой" ответ
     *  @return вектор, содержащий байты ответа устройства на данный запрос
     *  Вектор может быть пустым, если запрос был не успешен
     */
    virtual const std::vector<char>& rawAnsver() const { return ansver_data; }

    /** Режим подавления исключения при возникновении ошибки modbus
     *  @param enable
     *      true - Если во время выполнения метода @a exec произойдет ошибка
     *  будет выброшено соответствующее исключение\n
     *      false - Исключене будет подавлено (default)
     */
    virtual void enableExceptions(bool enable) { m_enableExceptions = enable; }

protected:
    virtual void setError(const modbusException& error) { m_error = error; }
    virtual void process(Connection* _io) = 0;
    virtual void protectBusy();

    virtual void saveModbussettings(Connection* _io);
    virtual void restoreModbussettings(Connection* _io);

    std::vector<char> ansver_data;
    enStatus m_status;

private:
    modbusException m_error;

    void *libmodbusStatus;

    bool m_enableExceptions;

    uint8_t slaveAddr;
    uint16_t startAddr;
    uint8_t count;

    bool useCRC;
};

}

#endif // REQUEST_H
