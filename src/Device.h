/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#ifndef DEVICE_H
#define DEVICE_H

#include <vector>
#include <typeindex>
#include <cstdint>

#include <SCTBModbusDevice.h>

namespace SCTBModbusDevice {

class request;
class Connection;
class AbstractCell;


/**
 * @class Device
 * Класс, представляющий устройсво сервер modbus
 */
class SCTB_MODBUS_DEVICE_EXPORT Device
{
    friend class DeviceFactory;
public:
    /**
     * @brief Создать полную копию устройства со всеми ячейками
     * @return Указатель на созданную копию устройства
     */
	virtual Device *clone() const;

    /**
     * @brief Установить транспортное соединение для устройства
     * @param connection транспортное соединение
     * Устройство не будет выполнять команды, пока не будет установлено
     * соединение
     */
    void setConnection(Connection* connection) { io = connection; }

    /**
     * @brief Получить идентификатор устройства
     *
     * Идентификатор уникален для каждого типа устройств
     * @return Идентификатор устройства
     */
    uint32_t id() const { return mID; }


    bool useCRC() const { return mUseCRC; }

    /**
     * @brief Установить флаг использования CRC16 при пердаче modbus
     *
     * CRC16 используется большинством устройств, однако некоторые производители
     * опускают эту часть протокола, автоматически узнать об этом невозможно
     * @param use \n
     *  true - использовать CRC16 (default)\n
     *  false - Посылка и проверка CRC16 отключены
     */
	virtual void setUseCRC(bool use);

    /**
     * @brief Получить класс устройства (датчик, исполнитель и тд..)
     * @return Класс устройства
     */
    virtual String Class() const { return mClass; }

    /**
     * @brief Получить описание устройства
     * @return Описание устройства
     */
    virtual String Description() const { return mDescription; }

    /**
     * @brief Получить адрес устройства
     * @return Адрес устройства на шине modbus
     */
    virtual uint8_t Address() const  { return mAddress; }

    /**
     * @brief Установить адрес устройства
     * @param addr новый адрес устройства на шине modbus
     */
    virtual void setAddress(uint8_t addr);

    /**
     * @brief изменить адрес устройства
     *
     * Запускает процедуру смены адреса устройством на указанный, если функция
     * поддерживается
     * @param newAddr Новый адрес устройства на шине modbus
     */
    virtual void changeAddress(uint8_t newAddr);

    /**
     * @brief Запускает процедуру принятия и сохранения изменений в устройстве
     */
    virtual void acceptChanges();

    /**
     * @brief Удаляет кеш всех ячеек устройства
     */
    virtual void invalideteAll();

    /**
     * @brief Выполнить запрос к устройству
     * @param req Объект-сапрос
     */
    virtual void execRequest(request &req);

    /**
     * @brief Получить список всех ячеек устройства
     * @return Список ячеек устройства
     */
    virtual const List<AbstractCell *> &Cells() const { return cells; }

    //@{
    /// @name Групповой доступ

    /**
     * @brief Синхронно прочитать группу ячеек
     *
     * Группа может содержать любое количество несмежных ячеек любого типа
     * Ячейки будут автоматически поделены на смежные группу, ячейки к которым
     * запрещен групповой доступ обрабатываются без группировки.
     * @param cells список ячеек, данные в которых необходимо обновить
     * @return
     *  true - операция успешно завершена\n
     *  false - во время выполнения возникли ошибки
     */
    virtual bool readCellGroup(List<AbstractCell *> &cells);

    /**
     * @brief Синхронно записать группу ячеек
     *
     * Группа может содержать любое количество несмежных ячеек любого типа
     * Ячейки будут автоматически поделены на смежные группу, ячейки к которым
     * запрещен групповой доступ обрабатываются без группировки.
     * @param cells список ячеек, данные в которых необходимо обновить
     * @return
     *  true - операция успешно завершена\n
     *  false - во время выполнения возникли ошибки
     */
    virtual bool writeCellGroup(List<AbstractCell *> &cells);

    //@}


    /// Деструктор
    virtual ~Device();

protected:
    Device() {}
    Device(const Device& pattern);
    virtual void CacheAcceptCells();

private:
    String mClass, mDescription;

    unsigned int maxSimulateneousCellsToRead;
    uint8_t mAddress;
    uint32_t mID;
	bool mUseCRC;
    bool passwordUpdated;

    List<AbstractCell*> cells;
    List<AbstractCell*> AcceptCellsCache;

    Connection *io;

    void invalideteGroup(List<AbstractCell *> &cells);

    request *requestFactory(bool RW, const List<AbstractCell *> cellLst);
    List<AbstractCell *> findSerie(List<AbstractCell *>::const_iterator &pos,
                                   const List<AbstractCell *>::const_iterator &end
                                   ) const;
    void RefreshAccessRights();
};

}

#endif // DEVICE_H
