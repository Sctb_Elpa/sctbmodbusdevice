/*
* This file is part of SCTBModbusDevice, an open-source cross-platform library
* Copyright (C) 2009  Shilo_XyZ_
*
* This library is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Contact Shilo_XyZ_:
*          e-mail:  SweetTreasure<at>2ch.hk
*/

#include <sstream>

#include <Device.h>

#include <abstractcell.h>

using namespace SCTBModbusDevice;

static Map<std::type_index, int> typesizemap;

void AbstractCell::setValue()
{
    if (IsReadonly())
        throw std::logic_error(
                "Cann't set value for cell with READONLY attribute.");
}

String AbstractCell::toString() const
{
    std::stringstream res;
    switch (Type()) {
    case INVALID:
        res << "INVALID";
        break;
    case HoldingRegister:
        res << "HoldingRegister";
        break;
    case InputRegister:
        res << "InputRegister";
        break;
    case Coil:
        res << "Coil";
        break;
    case DiscreteInput:
        res << "DiscreteInput";
        break;
    default:
        res << "UNKNOWN";
        break;
    }
    res << ";" << std::hex << "0x" << Address() << std::dec;
    res << ";" + String(ValueType().name());
    res << ";" << String(IsReadonly() ? "RO" : "RW");
    res << ";" << String(Serialisable() ? "ser" : "alone");
    res << ";"  << Category() + ";" + Name();
    if (!Description().empty())
        res << ";" + Description();
    return res.str();
}

bool AbstractCell::operator <(const AbstractCell &enother) const
{
    bool res = false;
    if (Type() == enother.Type())
        res = Address() < enother.Address();
    else
        res = Type() < enother.Type();

    return res;
}

bool AbstractCell::operator==(const AbstractCell &enother) const
{
    return (Address() == enother.Address()) &&
            (Type() == enother.Type()) &&
            (ValueType() == enother.ValueType());
}

void AbstractCell::createtypesizemap()
{
    if (typesizemap.empty())
    {
        typesizemap[typeid(uint16_t)] = sizeof(uint16_t);
        typesizemap[typeid(int16_t)] = sizeof(int16_t);
        typesizemap[typeid(uint32_t)] = sizeof(uint32_t);
        typesizemap[typeid(int32_t)] = sizeof(int32_t);
        typesizemap[typeid(bool)] = 1;
        typesizemap[typeid(float)] = sizeof(float);
        typesizemap[typeid(double)] = sizeof(double);
    }
}

int AbstractCell::typeInfoToSize(const std::type_index &info)
{
    return typesizemap[info];
}
